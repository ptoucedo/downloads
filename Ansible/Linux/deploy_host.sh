#!/bin/bash
#Skript um den Hostnamen eines Debian8 zuaendern, so wie die Anpassungen an die Entwicklung zu aendern


newhost=$1
ipaddr=$2

#Funktionen

function usage
{
		echo "usage: $0 [ [-Config] [-SSH] [-Puppet] [-Add] [-Create [login1] [login2] [login3] [...] ] [-Save] ]"
		echo "Das Skript muss nacheinander mit den Optionen in der folgenden Reihenfolge ausgefuehrt werden:"
		echo "1. -Config"
		echo "2. -SSH"
		echo "3. -Puppet"
		echo "4. -Add"
		echo "5. -Create"
		echo "6. -Save"
		echo ""
		echo "Beispielaufruf"
		echo "	Hostname und IP aendern:"
		echo "	new_host.sh -Config"
		echo ""
		echo ""
		echo "Beschreibung der Optionen"
		echo "-Config"
		echo "	Mit dieser Option wird der Hostname, die IP und die Konfigurationsdateien angepasst."
		echo ""
		echo "-SSH"
		echo "	Erneuert die SSH Hostkeys."
		echo "	Nach anpassen des Hostnamen notwendig."
		echo ""
		echo "-Puppet"
		echo "	Installiert die notwendigen Pakete fuer die Anbindung an einen Puppetserver."
		echo ""
		echo "-Add"
		echo "	Fuegt den Benutzer zur lokalen passwd hinzu."
		echo "	Benoetigt als zusaetzlicher Parameter das Kuerzel des Benutzers."
		echo ""
		echo "-Create"
		echo "	Erstellt die Verzeichnisse fuer die Logins."
		echo "	Benoetigt als zusaetzliche Parameter die Hauptlogins des Benutzers."
		echo ""
		echo "-Save"
		echo "	Kopiert/sichert alle notwendigen Konfigurationsdatein auf s3adm."
		echo ""
		echo ""
		echo "!!!Wichtiger Hinweis!!!"
		echo "Ausgabe der Optionen unbedingt beachten."
                echo ""
                echo ""
}

function valid_ip()
{
    local  ip=$1
    local  stat=1

    if [[ $ip =~ ^[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}$ ]]; then
        OIFS=$IFS
        IFS='.'
        ip=($ip)
        IFS=$OIFS
        [[ ${ip[0]} -le 255 && ${ip[1]} -le 255 \
            && ${ip[2]} -le 255 && ${ip[3]} -le 255 ]]
        stat=$?
    fi
    return $stat
}

function configure
{
	#Neuer Hostname und IP (Entwicklernetz TX) abfragen
	oldhost=dev-00


	#Hostnamen und IP aendern
	##/etc/hostname
	echo "/etc/hostname wird angepasst ..."
	sed -i "s/.*$HOSTNAME.*/$newhost/" /etc/hostname

	##/etc/hosts
	echo "/etc/hosts wird angepasst ..."
	sed -i "s/.*$HOSTNAME.*/$ipaddr		$newhost.abasag.intra		$newhost/" /etc/hosts

	##IP aendern
	echo "IP Addresse wird angepasst ..."
	sed -i "s/.*address 10\.168\..*/        address $ipaddr/" /etc/network/interfaces
			
	#Mountpoint 2.te HDD aendern
	##Mountpoint erstellen
	echo "Neuer Mountpoint wird erstellt ..."
	mkdir -p /a/$newhost/$newhost

	##2.te HDD aushaengen
	echo "2.te HDD wird ausgehaengt ..."
	umount /a/$HOSTNAME/$HOSTNAME
	rm -rf /a/$HOSTNAME

	##fstab aendern
	echo "Neuer Mountpoint wird in /etc/fstab eingetragen ..."
	sed -i "s!.*$HOSTNAME.*!/dev/mapper/data--partition-root /a/$newhost/$newhost	xfs	defaults 1	1!" /etc/fstab

	#NFS exports aendern
	echo "NFS exports werden angepasst ..."
	sed -i "s!.*$HOSTNAME.*!/a/$newhost/$newhost          *(rw,no_root_squash,no_all_squash,insecure,sync,no_subtree_check)!" /etc/exports

	#Samba Konfiguration anpassen
	##alte Konfig loeschen
	echo "Hostspezifische Samba Konfiguration wird angepasst ..."
	rm /etc/rc-abas.d/smb.conf.$HOSTNAME
        rm /etc/puppetOK

}

function create_ssh
{
		 rm /etc/ssh/ssh_host_*
		 dpkg-reconfigure openssh-server
		 echo "Host Keys wurden erstellt ..."
}

function puppet
{
	#Puppet-Repository installieren
	cd /tmp/
	wget https://apt.puppetlabs.com/puppetlabs-release-pc1-jessie.deb
	dpkg -i /tmp/puppetlabs-release-pc1-jessie.deb
	
	#Puppet-Agent installieren
	apt-get update && apt-get install puppet-agent
	/opt/puppetlabs/bin/puppet resource service puppet ensure=running enable=true
	
	#Aufraeumarbeiten
	rm -f /tmp/puppetlabs-release-pc1-jessie.deb

	echo "Die Benutzerlogins muessen noch hinzugefuegt werden."
	echo "Das Skript erneut mit dem Parameter -Add aufrufen."
	exit 0
}

function add_user
{
	rm -f /etc/passwd
	mv /etc/passwd.contrib /etc/passwd
	echo "Fuege Logins des Benutzers ${my_param[0]} hinzu ..."
	grep "^${my_param[0]}" /s3adm/general/etc/passwd >> /etc/passwd
	yes | pwck /etc/passwd /etc/shadow
	echo "Die Verzeichnisse muessen noch angelegt werden."
        echo "Das Skript erneut mit dem Parameter -Create aufrufen."
	exit 0
}

function create_directories
{
	echo "Verzeichnisse werden angelegt ..."
	for param in "${my_param[@]}"
	do
                echo "Verzeichnis: $param"
                cp -a /a/$HOSTNAME/$HOSTNAME/dummy/ /a/$HOSTNAME/$HOSTNAME/$param
                chown -R $param:s3 /a/$HOSTNAME/$HOSTNAME/$param
		ln -s /plugins/.vim/ /a/$HOSTNAME/$HOSTNAME/$param/.vim
	done

	rm -r /a/$HOSTNAME/$HOSTNAME/dummy/
        echo "Alle Konfigurationsdateien muessen noch gesichert werden!"
        echo "Das Skript erneut mit dem Parameter -Save aufrufen."
	exit 0
}

function save_config
{
	echo "Sicherung aller Konfigurationsdateien wird gestartet ..."
	cp2adm /root/.profile
	cp2adm /etc/fstab
	cp2adm /etc/ntp.conf
	cp2adm /etc/inittab
	cp2adm /etc/rc.local
	cp2adm /etc/exports
	cp2adm /etc/passwd
	cp2adm /etc/shadow
	cp2adm /etc/group
	cp2adm /etc/xinetd.d/datmod
	cp2adm /etc/xinetd.d/edp
	cp2adm /etc/samba/smb.conf
	cp2adm /etc/rc-abas.d/smb.conf
	cp2adm /etc/rc-abas.d/smb.conf.$HOSTNAME
	cd /etc/ssh
	for i in ssh*; do cp2adm $i;done
	
	echo "Alle Konfigurationen wurden gesichert."
	echo "Anpassungen sind abgeschlossen."
	echo "Folgenden Befehl ausfuehren:"
	echo "cat /dev/null > ~/.bash_history && history -c && exit"
}

#### Main ####

service docker stop
killall docker
configure
create_ssh
find /etc/puppetlabs/puppet/ssl -name dev-00.abasag.intra.pem -delete
rm -rf /etc/puppetlabs/puppet/ssl
/opt/puppetlabs/bin/puppet agent -t
reboot


exit

while [ "$1" != "" ]; do
    case $1 in
		-Config )		shift
					configure
						;;
		-SSH )			create_ssh
						;;
		-Puppet )		puppet
						;;
		-Add )			add_user
						;;
		-Create )		create_directories
						;;
		-Save )			save_config
						;;
		-h | --help )	usage
                        exit
                        ;;
		* )     		usage
						exit 1
    esac
    shift
done
